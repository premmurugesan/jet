<?php

namespace App\Http\Controllers;


use App\Models\User;

class HomeController extends Controller
{
    public function index()
    {
        if (!auth()->user()->tokenCan('user-details')) {
            return "User has no access";
        }
        return User::all();
    }
}
